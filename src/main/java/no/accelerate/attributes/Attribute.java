package no.accelerate.attributes;

import java.util.Objects;

public class Attribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    public Attribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attribute attribute = (Attribute) o;
        return strength == attribute.strength && dexterity == attribute.dexterity && intelligence == attribute.intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength, dexterity, intelligence);
    }
}
