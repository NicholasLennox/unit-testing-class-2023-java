package no.accelerate.calculator;

import no.accelerate.attributes.Attribute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    void setUpCalculator() {
        calculator =  new Calculator();
    }

    @Test
    void add_validInputs_ShouldReturnSum() {
        // Arrange
        double lhs = 1;
        double rhs = 1;
        double expected = lhs + rhs;
        // Act
        double actual = calculator.add(lhs, rhs);
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void divide_zeroDenominator_ShouldThrowArithmeticExceptionWithAppropriateMessage() {
        // Arrange
        double lhs = 1;
        double rhs = 0;
        String expected = "Cant divide by zero";
        // Act
        Exception exception =
                assertThrows(ArithmeticException.class,
                        () -> calculator.divide(lhs,rhs));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }
}