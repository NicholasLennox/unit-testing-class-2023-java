package no.accelerate.fizzbuzz;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzConverterTest {

    FizzBuzzConverter fb;

    @BeforeEach
    void setup() {
        fb = new FizzBuzzConverter();
    }

    @Test
    void convert_Number_ShouldReturnNumberAsString() {
        // Arrange
        int fbNumber = 1;
        String expected = "1";
        // ETC
        String actual = fb.convert(fbNumber);
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void convert_FizzNumber_ShouldReturnFizz() {
        // Arrange
        int fbNumber = 3;
        String expected = "Fizz";
        // ETC
        String actual = fb.convert(fbNumber);
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void convert_BuzzNumber_ShouldReturnBuzz() {
        // Arrange
        int fbNumber = 5;
        String expected = "Buzz";
        // ETC
        String actual = fb.convert(fbNumber);
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void convert_FizzBuzzNumber_ShouldReturnFizzBuzz() {
        // Arrange
        int fbNumber = 15;
        String expected = "FizzBuzz";
        // ETC
        String actual = fb.convert(fbNumber);
        // Assert
        assertEquals(expected,actual);
    }

}